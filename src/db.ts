/*
    Leetgetbot, a leet get stats bot for Telegram
    Copyright (C) 2018 Joonas Ulmanen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
    db.ts
    database functions
*/

import { Pool } from 'pg'

const db_uri = process.env.DATABASE_URL || 'not set'

if (db_uri === 'not set') {
  throw new Error('DATABASE_URL is not set!')
}

process.env.DATABASE_URL = '***OMITTED***'

const dbPool = new Pool({
  connectionString: db_uri
})

export async function updateStreaks(chatId: string, ids: string[]) {
  await dbPool.query('update leet_streaks set streak=0 where updated < now() - INTERVAL \'47 hours\'') // clear streaks that haven't been updated in 2 days
  await dbPool.query('update leet_streaks set streak=streak+1, best_streak=GREATEST(streak+1, best_streak), updated=now() where chatId=$1 and userId = ANY($2::text[])', [chatId, ids])
  await dbPool.query('update leet_streaks set streak=0 where updated < now() - INTERVAL \'23 hours\'') // clear streaks that just now got old
}

export async function newStreak(chatId: string, userId: string, fromName: string) {
  return dbPool.query('insert into leet_streaks (chatId, userId, fromName, streak, best_streak) values ($1, $2, $3, 0, 0)', [chatId, userId, fromName])
}

export async function getUser(userId: string, chatId: string) {
  return dbPool.query('select * from leet_streaks where userId=$1 and chatId=$2', [userId, chatId])
}

export interface chatStreak {
  fromName: string,
  streak: number,
  bestStreak: number
}

export async function getChatStreaks(chatId: string): Promise<chatStreak[]> {
  const result = await dbPool.query('select * from leet_streaks where chatId=$1 and streak > 0 order by streak desc, best_streak desc', [chatId])
  return result.rows.map((row) => {
    return {
      fromName: row.fromname,
      streak: row.streak,
      bestStreak: row.best_streak
    }
  })
}

export interface bestChatStreak {
  fromName: string,
  bestStreak: number
}

export async function getChatBestStreaks(chatId: string): Promise<bestChatStreak[]> {
  const result = await dbPool.query('select fromname, best_streak from leet_streaks where chatId=$1 and best_streak = (select max(best_streak) from leet_streaks where chatId=$1)', [chatId])
  return result.rows.map((row) => {
    return {
      fromName: row.fromname,
      bestStreak: row.best_streak
    }
  })
}
