/*
    Leetgetbot, a leet get stats bot for Telegram
    Copyright (C) 2018 Joonas Ulmanen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
    index.ts
    Main entrypoint
*/

import TelegramBot, { Message } from 'node-telegram-bot-api'
import * as cron from 'node-cron'
import { updateStreaks, getUser, newStreak, getChatStreaks, getChatBestStreaks } from './db';

const token = process.env.TOKEN || 'not set'
process.env.TOKEN = '***SECRET***'

if (token === 'not set') {
  throw new Error('TOKEN is not set!')
}

const bot = new TelegramBot(token, {polling: true})
const LEET_REGEX = /leet|1337/
const getters: {
  [chatId: string]: string[]
} = {}
const userChatPairs: {
  [userChatPairId: string]: boolean
} = {}

cron.schedule('38 13 * * *', async () => {
  try {
    for (const chatId in getters) {
      if (getters.hasOwnProperty(chatId)) {
        const distinctGetters = Array.from(new Set(getters[chatId]))
        await updateStreaks(chatId, distinctGetters)
        getters[chatId] = []
      }

      const currentStreaks = await getChatStreaks(chatId)
      const bestStreaks = await getChatBestStreaks(chatId)

      const currentStreaksStr = currentStreaks.map(v => `${v.fromName}: ${v.streak} (${v.bestStreak})`).join('\n')
      const bestStreaksStr = bestStreaks.map((streaker) => `${streaker.fromName}`).join(', ')

      await bot.sendMessage(chatId, `Leetin aika on ohi. Streakit:\n\n${currentStreaksStr}\nTop (${bestStreaks[0].bestStreak} streak): ${bestStreaksStr}`)
    }
  } catch (err) {
    reportProblemToDev(err)
  }
})

bot.on('message', async (msg: Message) => {
  const msgSentDate = new Date(msg.date*1000)

  try {
    await initializeUserChatPair(msg)
  
    if(msgSentDate.getHours() === 13 && msgSentDate.getMinutes() === 37) {
      await onLeetTime(msg)
    }
  } catch (err) {
    reportProblemToDev(err)
  }

})

function createUserChatId(msg: Message) {
  if(!msg.from) return ''
  return `${msg.from.id}${msg.chat.id}`
}

async function initializeUserChatPair(msg: Message) {
  if (!msg.from) return
  const ucpId = createUserChatId(msg)
  const chatId = msg.chat.id.toString(),
        userId = msg.from.id.toString()
  
  if (!userChatPairs[ucpId]) {
    const userDetails = await getUser(userId, chatId)
    if (userDetails.rowCount === 0) {
      await newStreak(chatId,userId, msg.from.username || msg.from.first_name)
    }
    userChatPairs[ucpId] = true
  }
}

async function onLeetTime(msg: Message) {
  if (!msg.text || !msg.from || !msg.text.toLowerCase().match(LEET_REGEX)) return
  const chatId = msg.chat.id.toString(),
        userId = msg.from.id.toString()
  getters[chatId] = getters[chatId] || []
  getters[chatId].push(userId)
}

function reportProblemToDev(err: any) {
  console.log(err)
  bot.sendMessage(62461364, `Leetgetbot error: ${err}`)
}
